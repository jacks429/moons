import sqlite3
import uuid
import datetime

from sqlite3 import Error


def sql_connection():

    try:

        con = sqlite3.connect('bot_database.db')

        return con

    except Error:

        print(Error)


def sql_table_create_table(con, platform):

    cursorObj = con.cursor()

    cursorObj.execute(f"CREATE TABLE {platform}(id integer PRIMARY KEY, mail text, user text,password text, date_created text)")

    con.commit()


def save_creds(mail, password, recover=None, phone=None):
    con = sql_connection()
    con_cur = sql_connection().cursor()

    con_cur.execute(f"INSERT INTO creds VALUES({uuid.uuid1()}, '{mail}', {password}, {recover}', '{phone}', '{str(datetime.datetime.now())}')")

    con.commit()


def save_bot_accounts(mail, password, recover, phone):

    con = sql_connection()
    con_cur = sql_connection().cursor()

    con_cur.execute(f"INSERT INTO creds VALUES({uuid.uuid1()}, '{mail}', {password}, {recover}', '{phone}', '{str(datetime.datetime.now())}')")

    con.commit()