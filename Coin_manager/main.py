from core import driver_controler, manage_accounts
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-e", '--excell', action='store_true',
                    required=True,help='the excell file to parse')
parser.add_argument('-p', '--proxy', action='store_true',
                    required=True, help='proxy text file')
parser.add_argument('-r', '--recovery', )
args = parser.parse_args()


def main():
    manage_accounts.csv_from_excel(args.excell)
    manage_accounts.add_proxies(args.proxy)

    with open('core/your_csv_file.csv', 'r') as f:
        lines = f.readlines()[1:]
        f.close()
        for line in lines:
            g_acc, passw, rec, ip, port = line.split(',')
            driver = driver_controler.FucetContoller(f"{str(ip)}:{str(port)}")
            if args.recovery:
                driver.veryify_google(g_acc, passw, rec)
            driver.coinpot_account(g_acc, passw)
            driver.bounus_bit(g_acc, passw)
            driver.driver.quit()
            manage_accounts.add_to_master()





if __name__ == '__main__':
    main()
