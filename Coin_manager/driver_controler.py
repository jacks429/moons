from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.proxy import Proxy, ProxyType

import time
import random
import string
import requests
from faker import Faker
from random_username.generate import generate_username


class FaucetBot:

    def __init__(self, xkey, xevil_server, proxy):
        prox = Proxy()
        prox.proxy_type = ProxyType.MANUAL
        prox.http_proxy = proxy
        cap = webdriver.DesiredCapabilities.FIREFOX
        prox.add_to_capabilities(cap)
        cap['marionette'] = True
        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override",
                               'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0')
        profile.set_preference("dom.webdriver.enabled", False)
        profile.set_preference('useAutomationExtension', False)
        self.driver = webdriver.Firefox(
            executable_path=r"C:\Users\clayt\PycharmProjects\Mooneater\misc\geckodriver.exe", capabilities=cap,
            firefox_profile=profile)
        self.driver.execute_script("return navigator.userAgent")
        self.xkey = xkey
        self.xevil_server = xevil_server

    def create_coinpot(self, email, passw):
        self.driver.get('https://coinpot.co/')
        self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[1]/button').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('//*[@id="RegisterEmailInput"]').send_keys(email)
        self.driver.find_element_by_xpath('//*[@id="RegisterPasswordInput"]').send_keys(passw)
        self.driver.find_element_by_xpath('//*[@id="RegisterConfirmPasswordInput"]').send_keys(passw)
        key = self.get_captcha_key()
        self.xevil(key, self.driver.current_url)
        self.driver.find_element_by_xpath('//*[@id="AcceptTermsInput"]').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div[3]/div/div/div[3]/button').click()
        self.google_login(email, passw)
        self.driver.get('https://mail.google.com/mail')
        # self.driver.find_element_by_xpath('//*[contains(@name, "Coin Pot")]').click()
        time.sleep(8)
        ele = self.driver.find_element_by_xpath('//*[contains(@email, "support@coinpot.co")]')
        self.driver.execute_script("arguments[0].click();", ele)
        time.sleep(3)
        self.driver.find_element_by_xpath(
            "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[3]/div[3]/div/div[2]/a").click()

    def create_bounus_bit(self, email, passw):
        self.driver.get('http://bonusbitcoin.com')
        self.driver.find_element_by_xpath('//*[@id="PageContent_RegisterButton"]').click()
        self.driver.find_element_by_xpath('//*[@id="RegisterEmailInput"]').send_keys(email)
        self.driver.find_element_by_xpath('//*[@id="RegisterPasswordInput"]').send_keys(passw)
        self.driver.find_element_by_xpath('//*[@id="RegisterConfirmPasswordInput"]').send_keys(passw)
        self.driver.find_element_by_xpath('//*[@id="AcceptTermsInput"]')
        self.driver.find_element_by_xpath('/html/body/div[5]/div/div/div[4]/button[2]')
        self.google_login(email, passw)
        self.driver.get('https://mail.google.com/mail')
        time.sleep(8)
        ele = self.driver.find_element_by_xpath('//*[contains(@email, "enquiries@bonusbitcoin.co")]')
        self.driver.execute_script("arguments[0].click();", ele)
        time.sleep(3)
        link = self.driver.find_element_by_xpath("//div[@id=':lq']//a[1]")
        self.driver.get(link.text)

    # not done
    def create_bit_fun(self, email, passw):
        self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[2]/div[1]/button').click()
        self.driver.find_element_by_xpath('//*[@id="RegisterEmailInput"]').send_keys(email)
        self.driver.find_element_by_xpath('//*[@id="RegisterPasswordInput"]').send_keys(passw)
        self.driver.find_element_by_xpath('//*[@id="RegisterConfirmPasswordInput"]').send_keys(passw)
        key = self.get_captcha_key()
        self.xevil(key, self.driver.current_url)
        self.driver.find_element_by_xpath('//*[@id="AcceptTermsInput"]').click()
        self.driver.find_element_by_xpath('/html/body/div[3]/div/div/div[4]/button[2]')
        self.google_login(email, passw)

    def coinpot_convert(self, passw):
        coins = ['bitcoincash', 'coinpottokens', 'dogecoin', 'litecoin']
        random.shuffle(coins)
        for coin in coins:
            url = f"https://coinpot.co/coin/{coin}/convert"
            self.driver.get(url)
            self.conversion(passw)

    def conversion(self, passw):
        self.driver.find_element_by_xpath("//a[contains(@data-bind,'click: convertMaximum')]").click()
        self.driver.find_element_by_xpath('//*[@id="ConvertPasswordInput"]').send_keys(passw)
        self.driver.find_element_by_xpath('//*[@id="ConvertButton"]').click()
        self.driver.find_element_by_xpath("//button[@type='button'][contains(.,'Confirm conversion')]").click()

        # not done

    def withdraw(self, wallet, passw):
        self.driver.find_element_by_xpath('//*[@id="WithdrawalWalletAddressInput"]').send_keys(wallet)
        self.driver.find_element_by_xpath('/html/body/div[4]/div/div/div[2]/form/div[12]/div[2]/a').click()
        self.driver.find_element_by_xpath('//*[@id="WithdrawalPasswordInput"]').send_keys(passw)
        key = self.get_captcha_key()
        self.xevil(key, self.driver.current_url)
        self.driver.find_element_by_xpath('//*[@id="WithdrawButton"]').click()

    def create_google_accounts(self):
            faker = Faker()
            gmail_user_name = self.generate_random_user()
            passw = str(self.generate_password())
            self.driver.get('https://accounts.google.com/signup/v2/webcreateaccount?flowName=GlifWebSignIn&flowEntry=SignUp')

            first_name = self.driver.find_element_by_xpath('//*[@id="firstName"]')
            first_name.send_keys(str(faker.first_name_male()))

            last_name = self.driver.find_element_by_xpath('//*[@id="lastName"]')
            last_name.send_keys(str(faker.last_name()))

            gmail = self.driver.find_element_by_xpath('//*[@id="username"]')
            gmail.send_keys(gmail_user_name)

            password = self.driver.find_element_by_xpath(
                '/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[3]/div[1]/div/div/div[1]/div/div[1]/div/div[1]/input')
            password.send_keys(passw)

            passw_confirm = self.driver.find_element_by_xpath(
                "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div[1]/input")
            passw_confirm.send_keys(str(passw))

            self.driver.find_element_by_xpath(
                '/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button').click()
            return gmail_user_name, password

    def google_login(self, user, passw):
        self.driver.get(
            "https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin")
        self.driver.find_element_by_xpath('//*[@id="identifierId"]').send_keys(user)

        self.driver.find_element_by_xpath(
            '/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/span/span').click()
        time.sleep(3)
        try:

            self.driver.find_element_by_xpath(
                '/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input').send_keys(
                passw)

        except TimeoutException:
            print("Loading took too much time!")
        self.driver.find_element_by_xpath(
            '/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/span/span').click()

    def xevil(self, site_key, website_url):
        print('Starting xevil')
        base_url_input = f"http://{self.xevil_server}/in.php?key={self.xkey}&method=userrecaptcha&googlekey={site_key}&pageurl={website_url}"
        session = requests.Session()
        id = session.get(base_url_input).text.split('|')[1]
        base_url_res = f"http://{self.xevil_server}/res.php?key={self.xkey}&action=get&id={id}"
        res = session.get(base_url_res)
        print("solving")
        while "NOT" in res.text:
            time.sleep(10)
            print("trying")
            res = session.get(base_url_res)
        else:
            print('solved')
            return res.text.split('|')[1]

    def get_captcha_key(self):
        tag = "data-fv-addons-recaptcha2-sitekey="
        captcha = self.driver.find_element_by_xpath("//*[contains(@data-fv-addons,'reCaptcha2')]")
        html = str(captcha.get_attribute('outerHTML')).split(" ")
        for string in html:
            if tag in string:
                return string.strip(tag).strip('"')

    @staticmethod
    def generate_password(length=8):

        LETTERS = string.ascii_letters
        NUMBERS = string.digits
        PUNCTUATION = string.punctuation

        printable = f'{LETTERS}{NUMBERS}{PUNCTUATION}'

        # convert printable from string to list and shuffle
        printable = list(printable)
        random.shuffle(printable)

        # generate random password and convert to string
        random_password = random.choices(printable, k=length)
        random_password = ''.join(random_password)
        return random_password

    @staticmethod
    def generate_random_user():
        random_num = random.randint(5000, 10000)
        return str(generate_username(1)[0]) + str(random_num)






