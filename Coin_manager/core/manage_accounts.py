import xlrd
import csv
import pandas as pd


def csv_from_excel(csv):
    wb = xlrd.open_workbook(csv)
    sh = wb.sheet_by_name('Sheet1')
    your_csv_file = open('temp.csv', 'w')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)
    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))
    your_csv_file.close()


def add_proxies(prox_file):

    f = open('temp.csv', 'r+')
    data = [item for item in csv.reader(f)]
    ip_column = ["IP",]
    port_column = ['Port',]
    with open(prox_file, 'r') as p:
        for line in p:
            ip, port = str(line).split(':')
            ip_column.append(ip)
            port_column.append(port.strip())

    new_data = []
    for i, item in enumerate(data):
        try:
            item.append(ip_column[i])
            item.append(port_column[i])
        except IndexError as e:
            item.append("placeholder")
        new_data.append(item)
    csv.writer(f).writerows(new_data)
    f.close()

def add_to_master():

    combined_csv = pd.concat(pd.read_csv('tmp.csv'))
    combined_csv.to_csv("master.csv", index=False, encoding='utf-8-sig')
