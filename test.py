from selenium import webdriver
import json
import pprint

#//a[contains(text(),'api')]


dlls_list = []
cap = webdriver.DesiredCapabilities.FIREFOX
cap['marionette'] = True
profile = webdriver.FirefoxProfile()
profile.set_preference("general.useragent.override",
                           'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0')
profile.set_preference("dom.webdriver.enabled", False)
profile.set_preference('useAutomationExtension', False)
driver = webdriver.Firefox(executable_path=r"C:\Users\clayt\PycharmProjects\Mooneater\misc\geckodriver.exe",
                                    capabilities=cap, firefox_profile=profile)
driver.execute_script("return navigator.userAgent")
driver.get("https://docs.microsoft.com/en-us/uwp/win32-and-com/win32-apis")
dlls = driver.find_elements_by_xpath("//a[contains(text(),'api')]")
for dll in dlls:
    dlls_list.append(dll.text)
dll_dict = {"api-ms-win-core-com-l1-1-1.dll": dlls_list}
pprint.pprint(dll_dict)
with open("results.json", 'w') as fp:
    json.dump(dll_dict, fp)
