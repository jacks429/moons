import time
import requests
import sys

from selenium import webdriver
from selenium.webdriver.common.proxy import Proxy


class FaucetBot:

    def __init__(self, xevil_server, xkey, proxy):
        prox = Proxy()
        prox.httpProxy = proxy
        cap = webdriver.DesiredCapabilities.FIREFOX
        cap['marionette'] = True
        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override",
                               'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0')
        profile.set_preference("dom.webdriver.enabled", False)
        profile.set_preference('useAutomationExtension', False)
        prox.add_to_capabilities(cap)
        self.driver = webdriver.Firefox(executable_path=r"C:\Users\clayt\PycharmProjects\Mooneater\misc\geckodriver.exe", capabilities=cap, firefox_profile=profile)
        self.driver.execute_script("return navigator.userAgent")
        self.xkey = xkey
        self.xevil_server = xevil_server

    def eat_moons(self, email, cookie, password):
        self.driver.get("https://startpage.com")
        self.driver.install_addon(r"C:\Users\clayt\PycharmProjects\Mooneater\misc\adblockultimate@adblockultimate.net.xpi")
        time.sleep(4)
        self.driver.switch_to_window(self.driver.window_handles[1])
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])
        self.sighn_in_claim(email, cookie)
        self.bit_fun(email, password)
        self.bonus_bit(email, password)

    def sighn_in_claim(self, email, cookie=False):
        moons = ['https://moondash.co.in/', 'http://MoonDoge.co.in/', 'http://moonbit.co.in/',
                 'http://moonliteco.in', 'http://moonbitcoin.cash/',]

        for moon in moons:

            self.driver.get(moon)
            time.sleep(5)

            if cookie is True:
                self.delete_cookies(moon)
                self.driver.execute_script("arguments[0].click();", cookie)
                claim = self.driver.find_element_by_xpath("//span[@data-bind='visible: canClaim()']")
                claim.click()
                self.claim_2()
            else:
                self.driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[1]/div[2]/div/button').click()
                email_butt = self.driver.find_element_by_xpath('//*[@id="SignInEmailInput"]')
                email_butt.send_keys(email)
                key = self.get_captcha_key_moons()
                solve = self.xevil(key, str(self.driver.current_url))
                self.driver.execute_script(f'document.getElementById("g-recaptcha-response").innerHTML="{solve}";')
                self.driver.find_element_by_xpath("//*[contains(@data-fv-addons,'reCaptcha2')]").submit()
                self.driver.find_element_by_xpath("/html/body/div[3]/div/div/div[3]/button").click()
                time.sleep(10)
                claim1 = self.driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[3]/div[2]/button/span[1]")
                claim1.click()
                self.claim_2()

    def claim_2(self):
        key = self.get_captcha_key_moons()
        solve = self.xevil(key, str(self.driver.current_url))
        self.driver.execute_script(f'document.getElementById("g-recaptcha-response").innerHTML="{solve}";')
        self.driver.find_element_by_xpath("//*[contains(@data-fv-addons,'reCaptcha2')]").submit()
        self.driver.find_element_by_xpath("//body[@class='modal-open']/div[@id='ClaimModal']/div[@class='modal-dialog modal-lg']/div[@class='modal-content']/div[@class='modal-footer']/button[1]").click()

    def bit_fun(self, mail, password):
        self.driver.get("https://bitfun.co/")
        self.driver.switch_to_window(self.driver.window_handles[0])
        time.sleep(2)
        sighn_in = self.driver.find_element_by_xpath("/html/body/div[1]/div[2]/div[2]/div[2]/button")
        sighn_in.click()

        email = self.driver.find_element_by_xpath('//*[@id="SignInEmailInput"]')
        email.send_keys(mail)

        passw = self.driver.find_element_by_xpath('//*[@id="SignInPasswordInput"]')
        passw.send_keys(password)

        key = self.get_fun_site_key()
        solve = self.xevil(key, str(self.driver.current_url))
        self.driver.execute_script(f'document.getElementById("g-recaptcha-response-1").innerHTML="{solve}";')
        self.driver.find_element_by_xpath("//form[@id='RegisterForm']").submit()
        self.driver.find_element_by_xpath("//button[@type='submit'][contains(text(),'Sign In')]").click()
        print('done with x_evil')

        self.driver.find_element_by_xpath("//span[@data-bind='visible: canClaim()']").click()
        key = self.get_captcha_key_moons()
        solve2 = self.xevil(key, self.driver.current_url)
        self.driver.execute_script(f'document.getElementById("g-recaptcha-response").innerHTML="{solve2}";')
        self.driver.find_element_by_xpath("/html/body/div[1]/div[1]/div[3]/div[2]/div/div/div[3]/button[2]").click()

    def bonus_bit(self, mail, password):
        self.driver.get('https://bonusbitcoin.co/')
        self.driver.switch_to_window(self.driver.window_handles[0])
        time.sleep(3)
        self.driver.find_element_by_xpath('//*[@id="PageContent_SignInButton"]').click()
        email = self.driver.find_element_by_xpath('//*[@id="SignInEmailInput"]')
        email.send_keys(mail)
        passw = self.driver.find_element_by_xpath('//*[@id="SignInPasswordInput"]')
        passw.send_keys(password)
        key = self.get_fun_site_key()
        solve = self.xevil(key, str(self.driver.current_url))
        self.driver.execute_script(f'document.getElementById("g-recaptcha-response-1").innerHTML="{solve}";')
        self.driver.find_element_by_xpath("//*[contains(@data-fv-addons,'reCaptcha2')]").submit()
        self.driver.find_element_by_xpath('/html/body/div[7]/div/div/div[4]/button[2]').click()
        time.sleep(2)
        key2 = self.get_captcha_key_moons()
        solve2 = self.xevil(key2, str(self.driver.current_url))
        self.driver.execute_script(f'document.getElementById("g-recaptcha-response").innerHTML="{solve2}";')
        self.driver.find_element_by_xpath("//*[contains(@data-fv-addons,'reCaptcha2')]").submit()
        butt = self.driver.find_element_by_xpath("/html/body/div[3]/div[2]/div[2]/form/button[2]")
        butt.location_once_scrolled_into_view
        butt.click()

    def delete_cookies(self, domains=None):
        cookies = self.driver.get_cookies()
        for cookie in cookies:
            if domains is not None:
                if str(cookie["domain"]) in domains:
                    cookies.remove(cookie)
            else:
                self.driver.delete_all_cookies()

    def xevil(self, site_key, website_url):
        print('Starting xevil')
        base_url_input = f"http://{self.xevil_server}/in.php?key={self.xkey}&method=userrecaptcha&googlekey={site_key}&pageurl={website_url}"
        session = requests.Session()
        id = session.get(base_url_input).text.split('|')[1]
        base_url_res = f"http://{self.xevil_server}/res.php?key={self.xkey}&action=get&id={id}"
        res = session.get(base_url_res)
        print("solving")
        while "NOT" in res.text:
            time.sleep(10)
            print("trying")
            res = session.get(base_url_res)
        else:
            print('solved')
            return res.text.split('|')[1]

    def get_captcha_key_moons(self):
        tag = "data-fv-addons-recaptcha2-sitekey="
        captcha = self.driver.find_element_by_xpath("//*[contains(@data-fv-addons,'reCaptcha2')]")
        html = str(captcha.get_attribute('outerHTML')).split(" ")
        for string in html:
            if tag in string:
                return string.strip(tag).strip('"')

    def get_fun_site_key(self):
        tag = "sitekey="
        captcha = self.driver.find_element_by_xpath('//*[@id="SignInForm"]')
        html = str(captcha.get_attribute('outerHTML')).split(" ")
        for string in html:
            if tag in string:
                return string.strip(tag).split('"')[1]


if __name__ == '__main__':
    bot = FaucetBot(sys.argv[1], sys.argv[2], sys.argv[3])
    bot.eat_moons(sys.argv[4], sys.argv[5], sys.argv[6])

